package assign.resources;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import assign.domain.Meeting;
//import assign.domain.UTCourse;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.junit.Test;

public class TestEavesdropResource extends TestCase {

	
	/* ===============================================================
	 * 		GET /myeavesdrop/meetings/solum/year/2014/count
	 * ===============================================================
	 */
	@Test
	public void testCountValidInputSolum() throws Exception {
		
		Client client = ClientBuilder.newClient();
		
		try {
			
			// Pass in the REST resource url and call GET on it; save the response
			Response response = client.target("http://localhost:8080/assignment6/myeavesdrop/meetings/solum/year/2014/count/")
		                 .request().get();
			String meetings = response.readEntity(String.class);
			
	        // Parse XML
			List<String> xmlContent = new ArrayList<String>();
	        Document doc = Jsoup.parse(meetings);
	        for (Element e : doc.getElementsByTag("count")) {
	        	String s = e.html();
	       	 	xmlContent.add(s);	 
	         }

	        // Run assertions
	        if (!xmlContent.contains("1"))
	        	throw new RuntimeException("Incorrect count: expected 1");
	        
	        response.close();
	        
		} finally {
	        client.close();
	    }
	}
	
	
	/* ===============================================================
	 * 	GET /myeavesdrop/meetings/solum_team_meeting/year/2013/count
	 * ===============================================================
	 */
	@Test
	public void testCountValidInputSolumTM() throws Exception {
		
		Client client = ClientBuilder.newClient();
		
		try {
			
			// Pass in the REST resource url and call GET on it; save the response
			Response response = client.target("http://localhost:8080/assignment6/myeavesdrop/meetings/solum_team_meeting/year/2013/count")
		                 .request().get();
			String meetings = response.readEntity(String.class);
			
	        // Parse XML
			List<String> xmlContent = new ArrayList<String>();
	        Document doc = Jsoup.parse(meetings);
	        for (Element e : doc.getElementsByTag("count")) {  
	        	String s = e.html();
	       	 	xmlContent.add(s); 
	         }
	     
	        // Run assertions
	        if (!xmlContent.contains("5"))
	        	throw new RuntimeException("Incorrect count: expected 5");
	        
	        response.close();
	        
		} finally {
	        client.close();
	    }
	}
	
	
	/* ===============================================================
	 * 			GET /myeavesdrop/meetings/solum/year/2014/
	 * ===============================================================
	 */
	@Test
	public void testGetMeetingsSolumValidInput() throws Exception {
	
		Client client = ClientBuilder.newClient();
		
		try {
			
			// Pass in the REST resource url and call GET on it; save the response
			Response response = client.target("http://localhost:8080/assignment6/myeavesdrop/meetings/solum/year/2014/")
		                 .request().get();
			String meetings = response.readEntity(String.class);
			
	        // Parse XML
			List<String> xmlContent = new ArrayList<String>();
	        Document doc = Jsoup.parse(meetings);
	        for (Element e : doc.getElementsByTag("meeting_name")) {    
	        	String s = e.html();
	       	 	xmlContent.add(s);	 
	         }
	     
	        // Run assertions
	        if (!xmlContent.contains("solum.2014-07-29-16.03.log.html"))
	        	throw new RuntimeException("Incorrect count: expected solum.2014-07-29-16.03.log.html");
	        
	        response.close();
	        
		} finally {
	        client.close();
	    }
	}
	
	
	/* ===============================================================
	 * 		GET /myeavesdrop/meetings/solum/year/2013/count
	 * ===============================================================
	 */
	@Test
	public void testGetMeetingsSolumIEmptyYear() throws Exception {
	
		Client client = ClientBuilder.newClient();
		
		try {
			
			// Pass in the REST resource url and call GET on it; save the response
			Response response = client.target("http://localhost:8080/assignment6/myeavesdrop/meetings/solum/year/2013/count")
		                 .request().get();
			String meetings = response.readEntity(String.class);
			
	        // Parse XML
			List<String> xmlContent = new ArrayList<String>();
	        Document doc = Jsoup.parse(meetings);
	        for (Element e : doc.getElementsByTag("count")) {  
	        	String s = e.html();
	       	 	xmlContent.add(s);	 
	         }
	     
	        // Run assertions
	        if (!xmlContent.contains("0"))
	        	throw new RuntimeException("Incorrect count: expected 0");
	        
	        response.close();
	        
		} finally {
	        client.close();
	    }
	}
	
	
}
