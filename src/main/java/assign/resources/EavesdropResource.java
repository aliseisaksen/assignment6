package assign.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.ServletContext;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import assign.domain.Meetings;
import assign.services.EavesdropService;
import assign.services.EavesdropServiceImpl;

@Path("/myeavesdrop")
public class EavesdropResource {

	EavesdropService eavesdropService;
	String password;
	String username;
	String dburl;
	
	
	public EavesdropResource(@Context ServletContext servletContext) {		
		dburl = servletContext.getInitParameter("DBURL");
		username = servletContext.getInitParameter("DBUSERNAME");
		password = servletContext.getInitParameter("DBPASSWORD");
		this.eavesdropService = new EavesdropServiceImpl(dburl, username, password);
	}

	
	/* ===============================================================
	 * 					GET - Count meetings
	 * ===============================================================
	 */
	@GET
	@Path("/meetings/{meeting}/year/{year}/count")
	@Produces("application/xml")
	public StreamingOutput countProject(@PathParam("meeting") String team_meeting_name, @PathParam("year") String year, InputStream is) throws Exception {

		// Error checks on input
		if( !( (team_meeting_name.equals("solum")) || (team_meeting_name.equals("solum_team_meeting")) ) )
			throw new BadRequestException();
		
		if( !( (year.equals("2013")) || (year.equals("2014")) || (year.equals("2015")) ) )
			throw new BadRequestException();
			
		final Meetings meetings = eavesdropService.getMeetingByNameAndYear(team_meeting_name, year);
		
		// 404 Not Found
		if(meetings.getMeetings() == null)
			throw new NotFoundException();

		// Format output
		meetings.setCount(meetings.getMeetings().size());
		System.out.println(meetings.getMeetings().size());
		meetings.setMeetings(null);
	
		return new StreamingOutput() {
        public void write(OutputStream outputStream) throws IOException, WebApplicationException {
        	outputMeetings(outputStream, meetings);
        }
		};
	}
	
	
	/* ===============================================================
	 * 					GET - Read meetings
	 * ===============================================================
	 */
	@GET
	@Path("/meetings/{meeting}/year/{year}")
	@Produces("application/xml")
	public StreamingOutput readProject(@PathParam("meeting") String team_meeting_name, @PathParam("year") String year, InputStream is) throws Exception {

		// Error checks on input
		if( !( (team_meeting_name.equals("solum")) || (team_meeting_name.equals("solum_team_meeting")) ) )
			throw new BadRequestException();
		
		if( !( (year.equals("2013")) || (year.equals("2014")) || (year.equals("2015")) ) )
			throw new BadRequestException();
			
		final Meetings meetings = eavesdropService.getMeetingByNameAndYear(team_meeting_name, year);
		
		// 404 Not Found
		if(meetings.getMeetings() == null)
			throw new NotFoundException();
		if(meetings.getMeetings().size() == 0)
			throw new NotFoundException();
		
		return new StreamingOutput() {
        public void write(OutputStream outputStream) throws IOException, WebApplicationException {
        	outputMeetings(outputStream, meetings);
        }
		};
	}
		
	
	/* ===============================================================
	 * 						Output Meeting
	 * ===============================================================
	 */
	protected void outputMeetings(OutputStream os, Meetings meetings) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Meetings.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(meetings, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
	
}
