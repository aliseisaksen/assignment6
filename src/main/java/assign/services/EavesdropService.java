package assign.services;

import assign.domain.Meeting;
import assign.domain.Meetings;

public interface EavesdropService {

	//public void EavesdropServiceImpl(String dbUrl, String username, String password);
	

	
	// read/GET from db
	public Meeting getMeeting(String meeting_name, String year) throws Exception;
	
	public Meetings getMeetingByNameAndYear(String team_meeting_name, String year) throws Exception;
	
	public Meeting getMeeting(int id) throws Exception;
	
}
