package assign.services;

import java.util.List;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import assign.domain.Meeting;
import assign.domain.Meetings;

public class EavesdropServiceImpl implements EavesdropService{

	
	private SessionFactory sessionFactory;

	
	
	String dbURL = "";
	String dbUsername = "";
	String dbPassword = "";
	DataSource ds;

	// DB connection information would typically be read from a config file.
	public EavesdropServiceImpl(String dbUrl, String username, String password) {
		this.dbURL = dbUrl;
		this.dbUsername = username;
		this.dbPassword = password;
		
		// A SessionFactory is set up once for an application
		sessionFactory = new Configuration()
        .configure() // configures settings from hibernate.cfg.xml
        .buildSessionFactory();
		
		ds = setupDataSource();
	}
	
	public DataSource setupDataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setUsername(this.dbUsername);
        ds.setPassword(this.dbPassword);
        ds.setUrl(this.dbURL);
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        return ds;
    }
	
	// read/GET from db
	public Meeting getMeeting(String meeting_name, String year) throws Exception {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Meeting.class).
        		add(Restrictions.eq("year", year));
		
		List<Meeting> meetings = criteria.list();
		System.out.println("MADE IT TO THE ENDDD!: " + meetings.toString());
		if (meetings.size() > 0) {
			System.out.println(meetings.get(0).toString());
			return meetings.get(0);			
		} else {
			System.out.println("ISSUE: EavesdropService line 66");
			return null;
		}
	}
	
	
	public Meetings getMeetingByNameAndYear(String team_meeting_name, String year) throws Exception {
		Session session = sessionFactory.openSession();		
		session.beginTransaction();
		
		Query query = session.createQuery("from Meeting where team_meeting_name = :name and year = :year");
		query.setParameter("name", team_meeting_name);
		query.setParameter("year", year);
		// You can replace the above to commands with this one
		// Query query = session.createQuery("from Student where studentId = 1 ");
		List<Meeting> meetingsList = (List<Meeting>) query.list();
		Meetings meetings = new Meetings();
		meetings.setMeetings(meetingsList);
		//Meeting  = (Student)list.get(0);
		
		return meetings;
	}
	

	public Meeting getMeeting(int id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	// OR
	
//	public Meeting getMeeting(int id) throws Exception {
//		Session session = sessionFactory.openSession();
//		
//		session.beginTransaction();
//		
//		Criteria criteria = session.createCriteria(Meeting.class).
//        		add(Restrictions.eq("id", id));
//		
//		List<Meeting> meetings = criteria.list();
//		
//		return meetings.get(0);		
//	}
}
