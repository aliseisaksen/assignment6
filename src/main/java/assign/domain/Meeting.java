package assign.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table( name = "meetings" )
@XmlAccessorType(XmlAccessType.FIELD)
public class Meeting {
	
	@XmlAttribute
	private Long id;
	
	@XmlElement
	private String team_meeting_name;
	@XmlElement
	private String year;
	@XmlElement
	private String meeting_name;
	@XmlElement
	private String link;
	
    public Meeting() {
    	// this form used by Hibernate
    }
    
    
    public Meeting(String team_meeting_name, String year, String meeting_name, String link) {
    	// for application use, to create new events
    	this.team_meeting_name = team_meeting_name;
    	this.year = year;
    	this.meeting_name = meeting_name;
    	this.link = link;
    }
    
    
    @Id    
	@GeneratedValue(generator="increment")    
	@GenericGenerator(name="increment", strategy = "increment")
    public Long getId() {
		return id;
    }

    private void setId(Long id) {
		this.id = id;
    }
    
    
    @Column(name="team_meeting_name")
    public String getTeamMeetingName() {
		return team_meeting_name;
    }

    public void setTeamMeetingName(String team_meeting_name) {
		this.team_meeting_name = team_meeting_name;
    }
    
    
    @Column(name="year")
	public String getYear() {
    	return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
	
    @Column(name="meeting_name")
	public String getMeetingName() {
    	return meeting_name;
	}

	public void setMeetingName(String meeting_name) {
		this.meeting_name = meeting_name;
	}
	
	
    @Column(name="link")
	public String getLink() {
    	return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
    
}
