package assign.domain;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "meetings")
public class Meetings {
	
	private List<Meeting> meetings = new ArrayList<Meeting>();
	
	@XmlElement
	private Integer count = null;
	
	
	public List<Meeting> getMeetings() {
		return meetings;
	}

	@XmlElement(name="meeting")
	public void setMeetings(List<Meeting> meetings) {
		this.meetings = meetings;
	}
	
	
	public Integer getCount() {
		return count;
	}
	
	public void setCount(int c) {
		this.count = c;
	}
	
}