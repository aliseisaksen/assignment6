package assign.etl;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import assign.domain.Meeting;
//import assign.domain.UTCourse;






import java.util.logging.*;

public class DBLoader {
	private SessionFactory sessionFactory;
	
	Logger logger;
	
	public DBLoader() {
		// A SessionFactory is set up once for an application
		sessionFactory = new Configuration()
                .configure() // configures settings from hibernate.cfg.xml
                .buildSessionFactory();
        
        logger = Logger.getLogger("EavesdropReader");
	}
	
	
	public void loadData(Map<String, List<String>> data, String source) throws Exception {
		logger.info("Inside loadData.");
		
		// Transform data into Meeting object to be loaded into DB
		String team_meeting_name;
		String year;
		String meeting_name;
		String link;
		
		for(Map.Entry<String, List<String>> entry : data.entrySet()) {
			year = entry.getKey();
			year = year.substring(0, year.length()-1);
			
			for (Iterator<String> iterator = entry.getValue().iterator(); iterator.hasNext();) {
				String absUrl = iterator.next();
				link = absUrl;
				String[] splitUrl = absUrl.split("/");

				team_meeting_name = splitUrl[4];
				meeting_name = splitUrl[6];

				Meeting newMeeting = new Meeting(team_meeting_name, year, meeting_name, link);
				addMeeting(newMeeting);
			}
		}
	}
	
	public Long addMeeting(Meeting m) throws Exception {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Long id = null;
		try {
			tx = session.beginTransaction();
			session.save(m);
		    id = m.getId();
		    tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
				throw e;
			}
		}
		finally {
			session.close();			
		}
		return id;
	}
	
}
