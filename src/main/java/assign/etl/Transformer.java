package assign.etl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.*;

import org.apache.commons.io.FilenameUtils;

public class Transformer {
	
	Logger logger;
	
	public Transformer() {
		logger = Logger.getLogger("Transformer");		
	}
	
	
	public Map<String, List<String>> transform(Map<String, List<String>> data, String source) {
		
		logger.info("Inside transform.");
		Map<String, List<String>> newData = new HashMap<String, List<String>>();

		String extension = "";
		
		// Find  names of all the files subject to priority order
		for(Map.Entry<String, List<String>> entry : data.entrySet()) {
			
			String key = entry.getKey();
			int extVal = 0;
		
			for (String value : entry.getValue()) {
				
				if(value.contains(".log.html"))
					extVal = 4;
				else if(value.contains(".log.txt"))
					if(extVal < 3)
						extVal = 3;
				else if(value.contains(".html"))
					if(extVal < 2)
						extVal = 2;
				else if(value.contains(".txt"))
					if(extVal < 1)
						extVal = 1;
			}
			
			switch(extVal) {
				case 4: extension = ".log.html"; break;
				case 3: extension = ".log.txt"; break;
				case 2: extension = ".html"; break;
				case 1: extension = ".txt"; break;
				default: System.out.println("No appropriate file extension found."); break;
			}
			
			int index = 0;
			for (Iterator<String> iterator = entry.getValue().iterator(); iterator.hasNext();) {
			    String fileName = iterator.next();
			    if (!fileName.contains(extension)) {
			        iterator.remove();
			    } else {
			    	// generate absolute link using filename
			    	data.get(key).set(index, source + key + fileName);
			    	index++;
			    }
			}

		}
		
		newData = data;
		
		return newData;
	}
}