package assign.etl;

import java.util.List;
import java.util.Map;

public class ETLController {
	
	EavesdropReader reader;
	Transformer transformer;
	DBLoader loader;
	
	public ETLController() {
		transformer = new Transformer();
		loader = new DBLoader();
	}
	
	public static void main(String[] args) {
		ETLController etlController = new ETLController();
		
		/* ===============================================================
		 * 	TO LOAD TABLE: Run as Java Application with create property in
		 *  hibernate.cfg.xml after dropping table
		 * ===============================================================
		 */
		
		String source = "http://eavesdrop.openstack.org/meetings/solum/";
		String source2 = "http://eavesdrop.openstack.org/meetings/solum_team_meeting/";
		etlController.performETLActions(source);
		etlController.performETLActions(source2);
	}

	public void performETLActions(String source) {		
		try {
			
			reader = new EavesdropReader(source);
			
			// Read data
			Map<String, List<String>> data = reader.readData();
			
			// Transform data
			Map<String, List<String>> transformedData = transformer.transform(data, source);
			
			// Load data
			loader.loadData(transformedData, source);
			

		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
