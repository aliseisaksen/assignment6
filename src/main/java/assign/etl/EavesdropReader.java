package assign.etl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.logging.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class EavesdropReader {
	
	String url;
	Logger logger; 
	URL eavesdropURL = null;
	
	public EavesdropReader(String url) {
		this.url = url; // http://eavesdrop.openstack.org/meetings/solum/
		
		logger = Logger.getLogger("EavesdropReader");
	}
	
	/* ===============================================================
	 * Return a map where the contents of map is a single entry:
	 * <this.url, List-of-parsed-entries>
	 * ===============================================================
	 */
	public Map<String, List<String>> readData() {
		
		logger.info("Inside readData.");
		Map<String, List<String>> data = new HashMap<String, List<String>>();
		
		try {			
			eavesdropURL = new URL(this.url);
			URLConnection connection = eavesdropURL.openConnection();
			
			// Call read and parse methods
			String readData = readDataFromEavesdrop(connection); //contains HTML with year anchors
			List<String> projectYears = parseOutput(readData);
			Map<String, List<String>> filesMap = gatherYearData(projectYears, this.url);
			
			data = filesMap;

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return data;
	}
	
	
	/* ===============================================================
	 * 			Returns HTML of the requested page
	 * ===============================================================
	 */
	public String readDataFromEavesdrop(URLConnection connection) {
		String retVal = "";
		try {
			BufferedReader in = new BufferedReader(
				new InputStreamReader(connection.getInputStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				retVal += inputLine;
			}
			in.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return retVal;
	}
	
	
	/* ===============================================================
	 * Returns List of all link elements: 
	 * [2014/, 2015/] ... [meeting_name1, meeting_name2, ...]
	 * Insert all links from HTML input parameter into a List<>
	 * ===============================================================
	 */
	public List<String> parseOutput(String inputString) {
		
		// Gather all links
		List<String> linkNames = new ArrayList<String>();
		Document doc = Jsoup.parse(inputString);
	    Elements links = doc.select("a");
	    
	    ListIterator<Element> iter = links.listIterator();
	    
	    // Skip header links (Name, Last Modified, Size, Description, Parent directory)
	    for(int i = 0; i < 5; i++)
	    {
	    	iter.next();
	    }
	    
	    // Insert link titles into linkNames List
	    while(iter.hasNext()) {
    		Element e = (Element) iter.next();
    		String s = e.html();
    		s = s.replace("#", "%23");
    		linkNames.add(s);
	    }
	    
		return linkNames;		
	}
		
	
	/* ===============================================================
	 * Returns Map of Years and Files: 
	 * {2014/=[meeting1, meeting 2, ...], 2015/=[meeting1, meeting2, ...]}
	 * ===============================================================
	 */
	public Map<String, List<String>> gatherYearData(List<String> projectYears, String baseURL) {
		
		String url = "";
		Map<String, List<String>> filesMap = new LinkedHashMap<String, List<String>>();
		
		try {			
			
			// Visit all years and count their data. Can be broken into 2 methods.
			for(int i = 0; i < projectYears.size(); i++) {
				url = baseURL + "/" + projectYears.get(i);
				eavesdropURL = new URL(url);
				URLConnection connection = eavesdropURL.openConnection();
				String readData = readDataFromEavesdrop(connection);
				List<String> projectFiles = parseOutput(readData);
				filesMap.put(projectYears.get(i), projectFiles);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filesMap;
	}
}
